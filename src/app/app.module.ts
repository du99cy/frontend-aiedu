import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { appInitializer } from '@core/initializer/app.initializer';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';
import { FootersComponent } from '@features/footers/footers.component';
import { HeadersComponent } from '@features/headers/headers.component';
import { LayoutsComponent } from '@features/layouts/layouts.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductComponent } from './pages/products/products.component';
import { ResearchComponent } from './pages/research/research.component';



@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    FootersComponent,
    LayoutsComponent,
    HomeComponent,
    CoursesComponent,
    ResearchComponent,
    ProductComponent,
    BlogsComponent,
    ContactComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide:APP_INITIALIZER,
      useFactory:appInitializer,
      multi:true,
      deps:[LanguageChangeSharingService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
