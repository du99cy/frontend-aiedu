import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  teacher_list: Teacher[] = [
    {
      name: 'Nguyen Xuan Hoai',
      specialize: 'Natural language processing. Operations research.',
      img: '1.jpeg',
    },
    {
      name: 'Nguyen Thi Thuy',
      specialize: 'Computer vision. Machine learning in agriculture.',
      img: '2.jpeg',
    },
    {
      name: 'Nguyen Hoang Huy',
      specialize: 'Statistical. Time-series analysis',
      img: '4.jpeg',
    },
    {
      name: 'Dinh Viet Sang',
      specialize: 'Computer vision. Machine Learning. Deep Learning.',
      img: '5.jpeg',
    },
    {
      name: 'Tran Van Long',
      specialize: 'Computational intelligence. Computer Graphics',
      img: '6.jpeg',
    },
    {
      name: 'Nguyen Do Van',
      specialize:
        'Machine Learning, Data Mining, Intelligent System and Robotics',
      img: '8.jpeg',
    },
    {
      name: 'Nguyen Minh Tien',
      specialize:
        'Natural language processing, machine learning, and deep learning',
      img: '11.png',
    },
    {
      name: 'Pham Van Cuong',
      specialize: 'Machine learning, IoT, Smart healthcare',
      img: '12.png',
    },
    {
      name: 'Nguyen Thanh Tung',
      specialize: 'Information systems and wireless sensor networks',
      img: '13.jpeg',
    },
    {
      name: 'Vu Hong Son',
      specialize:
        'Active noise cancellation, VLSI architecture design, System-on-chip design, and Object detection.',
      img: '14.jpeg',
    },
    {
      name: 'Nguyen Van Hau',
      specialize:
        'SAT, CSPs, Operations Research, Machine Learning, Natural Language Processing',
      img: '15.png',
    },
    {
      name: 'Vu Hai',
      specialize:
        'Medical images analysis; smart camera network; human computer interaction; CV in Agriculture',
      img: '16.jpeg',
    },
    {
      name: 'Dang Thai Viet',
      specialize: 'STEM / IT specialist, expert of mechatronics field.',
      img: '17.png',
    },
    {
      name: 'Nguyen Chi Thanh',
      specialize:
        'Natural Language Processing - Management and Information Systems Engineering',
      img: '18.jpeg',
    },
    {
      name: 'Tran Cao Truong',
      specialize: 'Computer Science - Natural Language Processing',
      img: '19.jpeg',
    },
    {
      name: 'Phan Thi Hai Hong',
      specialize:
        'Machine learning, Computer vision, Image processing, Action recognition',
      img: '20.jpeg',
    },
    {
      name: 'Thuy Nguyen',
      specialize: 'Multilingual IT expert, Digital Transformation',
      img: 'thuy_nguyen.jpg',
    },
    {
      name: 'Satoki Tsuyuri ',
      specialize:
        'Multilingual IT expert, Machine Learning, Business Intelligence',
      img: 'Satoki_Tsuyuri.jpg',
    },
  ];
  data_translate: any = {
    intro: {
      vi: 'Viện nghiên cứu ngoài công lập đầu tiên chuyên về đào tạo, nghiên cứu và phát triển công nghệ trong lĩnh vực Trí tuệ - Nhân tạo (AI), Khoa học dữ liệu (DS), Công nghệ thông tin và Tự động hóa ở Việt Nam.',
      us: 'The first non-public research institute specializing in training, researching, and the development of technology in the fields of Artificial Intelligence (AI), Data Science (DS), Information Technology and Automation in Vietnam.',
    },
    sub_content: {
      vi: [
        {
          title: 'Đào tạo',
          content:
            'Bồi dưỡng kiến thức và kỹ năng về lĩnh vực Trí tuệ - Nhân tạo, Khoa học dữ liệu và chuyển đổi số.',
        },
        {
          title: 'Tư vấn',
          content:
            'Chuyển giao công nghệ có ứng dụng Trí tuệ - Nhân tạo, Khoa học dữ liệu và chuyển đổi số.',
        },
        {
          title: 'Nghiên cứu',
          content:
            'Xây dựng các nền tảng, hệ thống công nghệ có ứng dụng Trí tuệ - Nhân tạo và phân tích dữ liệu lớn.',
        },
        {
          title: 'Cung cấp chuyên gia',
          content:
            'Tổ chức hội thảo sự kiện công nghệ có liên quan đến lĩnh vực Trí tuệ - Nhân tạo, Khoa học dữ liệu và chuyển đổi số.',
        },
      ],
      us: [
        {
          title: ' Training',
          content:
            'Fostering knowledge and skills in the field of Artificial - Intelligence, Data Science, and digital transformation.',
        },
        {
          title: 'Consulting',
          content:
            'Technology transfer application with Artificial Intelligence, Data Science and digital transformation. .',
        },
        {
          title: 'Research',
          content:
            'Building technology platforms and systems with application of Artificial Intelligence and big data analysis.',
        },
        {
          title: 'Provide expert',
          content:
            'Organizing technology events and seminars related to the field of Artificial - Intelligence, Data Science and digital transformation..',
        },
      ],
    },
    content: {
      vi: 'Tự hào được lãnh đạo bộ Khoa học Công nghệ nêu tên là một trong 10 đơn vị tiêu biểu về AI tại Việt Nam trong sự kiện AI DAY thuộc chuỗi sự kiện của cuộc gặp mặt giữa 100 nhà khoa học tiêu biểu về lĩnh vực thuộc CMCN 4.0 với Chính phủ Việt Nam, do Bộ Khoa học Công nghệ, Bộ Kế hoạch Đầu tư tổ chức vào tháng 8/2018.',
      us: 'Honored to be mentioned by the head of the Ministry of Science and Techonology as on one of the 10 examplary units of Vietnam in the field of AI at the AI DAY event, an event part of a series of events organized by the Ministry of Science and Technology and the Ministry of Planning and Investment in August 2018 for the Government of Vietnam to meet up with 100 outstanding scientists in the field of the Industrial Revolution. 4.0.',
    },
  };

  language_name:string = 'us'
  constructor(private LanguageService:LanguageChangeSharingService){
    //real time when user change another language
    this.LanguageService.getLanguageObservable().subscribe((language_name:string)=>
    {
        this.language_name = language_name
    })
  }
}

interface Teacher {
  name: string;
  specialize: string;
  img: string;
}
