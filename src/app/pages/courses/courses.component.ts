import { Component } from "@angular/core";
import { LanguageChangeSharingService } from "@core/services/languageChange-sharing.service";

@Component({
    selector:'app-courses',
    styleUrls:['./courses.component.scss'],
    templateUrl:'./courses.component.html'
})
export class CoursesComponent{
    data_translate:any = {
        intro:{
            title:{
                vi:'Chương trình đào tạo',
                us:'Training Program'
                    },
            content:{
                vi:'Chúng tôi gồm các nhà nghiên cứu, giảng viên, chuyên gia ngành, doanh nghiệp kết hợp lại nhằm tạo dựng môi trường học tập và ứng dụng Trí tuệ nhân tạo để cùng sáng tạo và đổi mới. Chúng tôi xây dựng các khóa học, tư vấn giải pháp công nghệ, trực tiếp phát triển các sản phẩm AI và đưa vào áp dụng triển khai thực tế.',
                us:'We are a group of researchers, lecturers, industry experts, and businesses that assemble to create a learning environment that applies Artificial Intelligence to create and innovate. We build courses, consult technology solutions, directly develop AI products, and implement them in the real world.'
            },
            
        },
        content:[{
            title:{
                vi:'THỊ GIÁC MÁY TÍNH',
                us:'COMPUTER VISION'
            },
            price:{
                vi:'5.000.000đ/15 buổi',
                us:'5.000.000đ/15 day'
            },
            content:{
                vi:`<p class="list-group-item" style="font-size: 14px">
                Khóa học gồm 2 phần chính. Nội dung chính thứ nhất giúp học viên được
                tiếp cận các khái niệm và kiến thức cơ bản về Xử lý ảnh. Nội dung
                chính thứ hai gồm các kỹ thuật và phương pháp hiểu ảnh ở mức độ ngữ
                nghĩa. Trong đó, các bài toán chính của thị giác máy tính như phát
                hiện, nhận dạng đối tượng, phân đoạn ảnh sẽ được trình bày.
              </p>
              <p class="list-group-item" style="font-size: 14px">
                Học viên được tiếp cận và thực hành với các ứng dụng của Xử lý ảnh và
                Thị giác máy tính với nhiều ví dụ minh họa, dữ liệu,và dự án từ các
                bài toán thực tế như phát hiện làn đường ứng dụng cho xe không người
                lái, khâu nối ảnh panorama, tìm kiếm ảnh theo nội dung (image
                retrieval),phân loại ảnh, phát hiện và phân loại đối tượng, phát
                hiện khuôn mặt, phát hiện biển số xe, phân loại phương tiện giao
                thông bằng camera thông minh, phát hiện đột nhật trong các ứng dụng
                nhà thông minh, phát hiện hành vi bất thường nơicông cộng, phân tích
                và nhận dạng ảnh khuôn mặt.
              </p>`,
              us:`<p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> The course consists of 2 main parts. The first part exposes students to basic concepts and knowledge of Image Processing. The second part teaches techniques and methods for understanding images on a semantic level. In it, the core content of computer vision such as detection, object recognition, image segmentation will be presented. </p>
              <p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> Students gain access to and get to practice with applications that use Image Processing and Computer Vision. A variety of examples, data, and projects taken from real-world problems will be presented to enhance the lesson. Some examples of real-world problems being tackled are: lane detection for driverless cars, panorama stitching, image retrieval, image classification, object detection and classification, face detection, license plate detection, vehicle classification by smart cameras, intrusion detection in smart home applications, abnormal behavior detection in public places, facial image analysis, and recognition. </p>  
              `
            }
            
        },
        {
            title:{
                vi:'HỌC MÁY CƠ BẢN VÀ ỨNG DỤNG',
                us:'Machine Learning Fundamentals and Applications',
            },
            price:
            {
                vi:'4.500.000đ/15 buổi',
                us:'4.500.000đ/15 day',
            },
            content:{
                vi:'<p _ngcontent-qfg-c23="" class="list-group-item" style="font-size: 14px;"> Học viên được tiếp cận các khái niệm, kỹ thuật, và mô hình học máy cơ bản và phổ dụng trong học có giám sát (Supervised learning) như hồi quy logistic (Logistic Regression), cây quyết định (Decision Tree), Support Vector Machines, học không giám sát (Unsupervised learning) như các kỹ thuật phân cụm (clustering). </p><p _ngcontent-qfg-c23="" class="list-group-item" style="font-size: 14px;"> Học viên được tiếp cận với các ứng dụng của học máy với nhiều ví dụ minh họa, dữ liệu, và dự án từ các bài toán thực tế như dự đoán/dự báo giá cả, đánh giá tín dụng cá nhân, phân loại phương tiện giao thông/ảnh/tin tức, phát hiện thâm nhập trong an ninh mạng,…Với cách tiếp cận dựa trên dự án (project-based) khóa đào tạo sẽ giúp người học có được các kỹ năng thực hành và giải quyết các vấn đề thực tế sử dụng các kỹ thuật và công cụ của học máy. </p>',
                us:'<p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> Students gain access to fundamental and universal machine learning concepts, techniques, and models in supervised learning approaches such as Logistic Regression, Decision Tree, and Support Vector and unsupervised learning approaches such as clustering techniques. </p><p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> Students learn the applications of machine learning through many examples, data, and projects from real-world problems such as price prediction/forecast, personal credit rating, method classification traffic/photo/news, intrusion detection in cybersecurity, etc. The project-based approach help students to acquire practical skills and solve real-world problems using machine learning techniques and tools. </p>'
            }
        },
        {
            title:{
             vi:'Học sâu',
             us:'Deep learning'   
            },
            price:{
                vi:'5.000.000đ/15 buổi',
                us:'5.000.000đ/15 day'
            },
            content:{
                vi:'<p _ngcontent-qfg-c23="" class="list-group-item" style="font-size: 14px;"> Khóa học trang bị kiến thức toàn diện cho người học trên 3 phần. Phần mạng nơ-ron nhân tạo bao gồm các kiến thức về tính toán trên mạng nơ-ron, các thuật toán huấn luyện mạng nơ-ron và các phương pháp tối ưu mạng được sử dụng nhiều trong thực tiễn. Phần mô hình DL giới thiệu các mô hình DL nổi tiếng được ứng dụng nhiều trên thế giới. Phần học biểu diễn (Representation Learning) mang đến cho người học xu hướng mới trong việc sử dụng deep learning cho việc trích xuất thông tin cho mọi loại dữ liệu làm tăng sự thông minh của hệ thống. </p><p _ngcontent-qfg-c23="" class="list-group-item" style="font-size: 14px;"> Học viên cũng có cơ hội tiếp cận với các ứng dụng thị giác máy như nhận dạng hình ảnh, làm cho máy tính có khả năng vẽ ảnh nghệ thuật; các ứng dụng xử lý ngôn ngữ tự nhiên như phân tích cảm xúc (sentiment analysis), phân tích mạng xã hội… </p>',
                us:'<p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> The course equips students with comprehensive knowledge through 3 sections. The artificial neural network section includes knowledge of neural network computations, neural network training algorithms, and network optimization methods widely used in practice. The DL model section introduces famous DL models that are widely applied in the world. The representation learning section gives students new ways to use deep learning for information extraction for all types of data increasing the intelligence of the system. </p><p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> Students have opportunities to access machine vision applications such as image recognition, allowing computers capability to draw artwork; natural language processing applications such as sentiment analysis, social network analysis, etc. </p>'
            }
        },
        {
            title:{
             vi:'XỬ LÝ NGÔN NGỮ TỰ NHIÊN',
             us:'Natural Language Processing'   
            },
            price:{
                vi:'5.000.000đ/15 buổi',
                us:'5.000.000đ/15 day'
            },
            content:{
                vi:'<p _ngcontent-qfg-c23="" class="list-group-item" style="font-size: 14px;"> Khóa học sẽ trang bị cho học viên những kỹ thuật cần thiết cho việc khai phá và phân tích dữ liệu văn bản để tìm ra những thông tin hữu ích để hỗ trợ cho việc đưa ra quyết định của các hệ thống. Thông qua những dự án thực tế, người học sẽ được đào tạo để có kỹ năng lập trình, phân tích, tổng hợp dữ liệu văn bản từ nhiều nguồn dữ liệu khác nhau. Nội dung chính sẽ được giảng dạy trong khóa học: <br _ngcontent-qfg-c23=""> - Đọc dữ liệu từ nhiều nguồn khác nhau <br _ngcontent-qfg-c23=""> - Trích xuất dữ liệu text từ các website <br _ngcontent-qfg-c23=""> - Trích xuất và khai phá dữ liệu text từ các mạng xã hội như Facebook và Twitter <br _ngcontent-qfg-c23=""> - Phân tích cảm xúc từ dữ liệu text <br _ngcontent-qfg-c23=""> - Phân tích quan điểm từ dữ liệu text <br _ngcontent-qfg-c23=""> - Tóm tắt văn bản </p><p _ngcontent-qfg-c23="" class="list-group-item" style="font-size: 14px;"> Học viên được tiếp cận với các ứng dụng của học máy với nhiều ví dụ minh họa, dữ liệu, và dự án từ các bài toán thực tế như dự đoán/dự báo giá cả, đánh giá tín dụng cá nhân, phân loại phương tiện giao thông/ảnh/tin tức, phát hiện thâm nhập trong an ninh mạng,…Với cách tiếp cận dựa trên dự án (project-based) khóa đào tạo sẽ giúp người học có được các kỹ năng thực hành và giải quyết các vấn đề thực tế sử dụng các kỹ thuật và công cụ của học máy. </p>',
                us:'<p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> The course will equip students with the necessary techniques for mining and analyzing textual data to find insight to support decision-making. In the practical projects, students will be trained in programming, analyzing, and the synthesization of textual data from many different data sources. Main content inlcudes: Syllabus: <br _ngcontent-qfg-c18=""> - Reading data from various sources <br _ngcontent-qfg-c18=""> - Extracting text data from websites <br _ngcontent-qfg-c18=""> - Extracting and mining text data from social networks such as Facebook and Twitter <br _ngcontent-qfg-c18=""> - Analysis Emotions from text data <br _ngcontent-qfg-c18=""> - Perspective analysis from text data <br _ngcontent-qfg-c18=""> - Text summary </p><p _ngcontent-qfg-c18="" class="list-group-item" style="font-size: 14px; text-align: justify;"> Students learn the applications of machine learning through many examples, data, and projects from real-world problems such as price prediction/forecast, personal credit rating, method classification traffic/photo/news, intrusion detection in cybersecurity, etc. The project-based approach help students to acquire practical skills and solve real-world problems using machine learning techniques and tools. </p>'
            }
        }
    ]

    }
    

    language_name:string = 'vi'
    constructor(private LanguageService:LanguageChangeSharingService){
        this.LanguageService.getLanguageObservable().subscribe((language_name:string)=>
        {
            this.language_name = language_name
        })
    }
}