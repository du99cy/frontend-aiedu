import { Component } from "@angular/core";
import { LanguageChangeSharingService } from "@core/services/languageChange-sharing.service";

@Component({
    selector:'app-blogs',
    templateUrl:'./blogs.component.html',
    styleUrls:['./blogs.component.scss']
})
export class BlogsComponent
{
    data_translate:any = {
        intro:{
            title:
            {
                vi:'Đối tác',
                us:'Partner'
            },
            content:{
                vi:'Chúng tôi gồm các nhà nghiên cứu, giảng viên, chuyên gia ngành, doanh nghiệp kết hợp lại nhằm tạo dựng môi trường học tập và ứng dụng Trí tuệ nhân tạo để cùng sáng tạo và đổi mới. Chúng tôi xây dựng các khóa học, tư vấn giải pháp công nghệ, trực tiếp phát triển các sản phẩm AI và đưa vào áp dụng triển khai thực tế.',
                us:'We are a group of researchers, lecturers, industry experts, and businesses that assemble to create a learning environment that applies Artificial Intelligence to create and innovate. We build courses, consult technology solutions, directly develop AI products, and implement them in the real world.'
            }
        },
        content:[
            {
                img:'20200722_141454.jpg',
                title:{
                    vi:'Cục kỹ thuật nghiệp vụ - Bộ Công An',
                    us:'Department of Technology - Ministry of Public Security'
                }
            },
            {
                img:'121149029.jpg',
                title:{
                    vi:'Viện nghiên cứu dữ liệu lớn - Vin BigData',
                    us:'Big Data Research Institute - Vin BigData'
                }
            },
            {
                img:'trung-tam-an-ninh-mang-viettel.jpg',
                title:{
                    vi:'Trung tâm an ninh mạng Viettel',
                    us:'Viettel Cybersecurity Center'
                }
            },
            {
                img:'timestudio-190120-019.jpg',
                title:{
                    vi:'Đội ngũ CTO - NextTech Group',
                    us:'CTO Team - NextTech Group'
                }
            },
            {
                img:'timestudio-190106-020.jpg',
                title:{
                    vi:'AI for CEOs - NextTech Group',
                    us:'AI for CEOs - NextTech Group'
                }
            },
            {
                img:'DSC06950.JPG',
                title:{
                    vi:'Sở thông tin và truyền thông',
                    us:'Department of Information and communication'
                }
            }
        ]

    }

    language_name:string = 'vi'
    constructor(private LanguageService:LanguageChangeSharingService)
    {
        this.LanguageService.getLanguageObservable().subscribe((language_name:string)=>
        {
            this.language_name = language_name
        })
    }
}