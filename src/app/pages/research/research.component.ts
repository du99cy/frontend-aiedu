import { Component } from '@angular/core';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.scss'],
})
export class ResearchComponent {
  data_translate:any = {
    intro: {
      title: {
        vi: 'Nghiên cứu',
        us: 'Research',
      },
      content: {
        vi: 'Chúng tôi gồm các nhà nghiên cứu, giảng viên, chuyên gia ngành, doanh nghiệp kết hợp lại nhằm tạo dựng môi trường học tập và ứng dụng Trí tuệ nhân tạo để cùng sáng tạo và đổi mới. Chúng tôi xây dựng các khóa học, tư vấn giải pháp công nghệ, trực tiếp phát triển các sản phẩm AI và đưa vào áp dụng triển khai thực tế',
        us: 'We are a group of researchers, lecturers, industry experts, and businesses that assemble to create a learning environment that applies Artificial Intelligence to create and innovate. We build courses, consult technology solutions, directly develop AI products, and implement them in the real world.',
      },
    },
    field_list: [
      { vi: 'Ngôn ngữ tự nhiên (NLP)', us: 'Natural Language Processing (NLP)' },
      { vi: 'Thị giác máy tính (CV)', us: 'Computer Vision (CV)' },
      { vi: 'Khoa học dữ liệu (DS)', us: 'Data Science (DS)' },
      { vi: 'Học máy sâu (DML)', us: 'Deep Machine Learning (DML)' },
    ],
    nlp:{
        title:{
            vi:'XỬ LÝ NGÔN NGỮ TỰ NHIÊN',
            us:'Natural Language Processing'
        },
        content:{
        vi:[
            '<div class="panel active fade-left" style="animation-duration: 300ms; transition-timing-function: ease; transition-delay: 0ms;"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Nghiên cứu phát triển công cụ tự động phân loại tin tức dạng văn bản với chủ đề được tổ chức theo dạng cây (tiếng Việt và tiếng Anh).</li><li><i class="fa-li fa fa-check"></i> Hệ thống có khả năng lọc và lấy thông tin cần thiết theo chủ đề, đặc tính thông tin cần.</li><li><i class="fa-li fa fa-check"></i> Cho phép cấu hình lấy tin từ nhiều nguồn, đa ngôn ngữ và chuyển đổi ngôn ngữ cho người dùng.</li><li><i class="fa-li fa fa-check"></i> Hệ thống có xây dựng cơ chế tìm kiếm thông tin nội dung.</li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Các tin tức được xử lý, phân tích, phân cụm và phân loại đưa vào mục mà nội dung tin chứa đựng.</li><li><i class="fa-li fa fa-check"></i> Nghiên cứu phát triển công cụ tự động tóm tắt các ý chính của một văn bản tiếng Việt thành một đoạn tin ngắn và nghiên cứu phát triển công cụ tự động tổng hợp các ý chính từ nhiều văn bản tiếng Việt cùng chủ đề thành một văn bản duy nhất. </li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Các tin tức được đánh giá định lượng theo mức độ ảnh hưởng để chủ đề, đặt tính nội dung hàm chứa trong tin.</li><li><i class="fa-li fa fa-check"></i> Các thông tin được số hóa kèm theo chuỗi thời gian thực tế của tin, vị trí tin qua đó giúp nhà phân tích tin thấy giá trị nội tại nội dung tin.</li><li><i class="fa-li fa fa-check"></i> Nghiên cứu phát triển công cụ tự động phân tích sắc thái của văn bản (tiếng Việt và tiếng Anh). Đánh giá sắc thái của văn bản có nhiều ứng dụng trong thực tiễn như: đánh giá phản hồi của người dùng để đưa ra cải tiến sản phẩm phù hợp; hay dự báo về xu hướng về chính trị, kinh tế, tài chính.</li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Các công nghệ lõi của AI Academy VietNam đã được đưa vào để xây dựng các ứng dụng thu thập, phân tích, đánh giá tin tức kết hợp với phân tích số liệu cho các thị trường thương mại quốc tế như thị trường thủy sản.</li></ul></div>'
        ],
        us:[
            '<div class="panel active"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Research and develop tools to automatically classify text-based news by topics organized under a hierarchy tree (Vietnamese and English). </li><li><i class="fa-li fa fa-check"></i> Systems capable of filtering and retrieving the necessary information by subject and characteristics. </li><li><i class="fa-li fa fa-check"></i> Allows configuration to get information from multiple sources, multi-languages, &ZeroWidthSpace;&ZeroWidthSpace;and changes languages &ZeroWidthSpace;&ZeroWidthSpace;for users. </li><li><i class="fa-li fa fa-check"></i> Systems with built-in content search mechanism. </li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> he news is processed, analyzed, clustered, and classified into the category that contains them. </li><li><i class="fa-li fa fa-check"></i> Research and develop automation tools that summarize the main ideas of a document into a short message as well as automation tools to synthesize main ideas from multiple documents on the same topic into a single document. (Vietnamese documents) </li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i>The news is evaluated quantitatively according to the level of influence on the topic, set the content contained in the news. </li><li><i class="fa-li fa fa-check"></i> Information is digitized with its timestamp and location to help data analysts gather insights. </li><li><i class="fa-li fa fa-check"></i> Research and develop tools to automatically analyze nuances of text (Vietnamese and English). Text nuance evaluation has many practical applications such as: assessing user feedback to make appropriate product improvements; or forecasting political, economic, and financial trends. </li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Ai Academy Vietnam\'s core technologies have been incorporated into building applications to collect, analyze, and evaluate news in combination with data analysis for international commercial markets such as the seafood market. </li></ul></div>'
        ]},
        tree:{
            vi:[
                'Phân loại tin tức tổ chức theo dạng cây',
                'Tóm tắt văn bản',
                'Phân tích sắc thái của văn bản',
                'Sản phẩm ứng dụng NLP'
            ],
            us:[
                'Clustering news by hierarchy tree',
                'news summarization',
                'news sentiment',
                'nlp application product'
            ]
        },
    },
    
    cv:{
        title:{
            vi:'CÔNG NGHỆ THỊ GIÁC MÁY TÍNH',
            us:'Computer Vision'
        },
        content:{
            vi:'<ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Các công nghệ nhận dạng ảnh.</li><li><i class="fa-li fa fa-check"></i> Camera thông minh trong giám sát giao thông.</li><li><i class="fa-li fa fa-check"></i> Các công nghệ nhận dạng hành vi khách hàng cho camera thông minh dùng trong bán lẻ.</li><li><i class="fa-li fa fa-check"></i> Các công nghệ nhận dạng cử chỉ bàn tay.</li><li><i class="fa-li fa fa-check"></i> Các công nghệ nhân dạng và đo mực nước (sông, biển) thông qua camera.</li><li><i class="fa-li fa fa-check"></i> Các giải pháp nhận dạng ORC, bóc tách thông tin theo khuôn dạng định sẵn từ ảnh và video.</li><li><i class="fa-li fa fa-check"></i> Các công nghệ định danh qua ảnh, video.</li></ul>',
            us:'<ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Image recognition technologies. </li><li><i class="fa-li fa fa-check"></i> Smart camera in traffic monitoring. </li><li><i class="fa-li fa fa-check"></i> Customer behavior recognition technologies for smart cameras in retail stores. </li><li><i class="fa-li fa fa-check"></i> Hand gesture recognition technologies. </li><li><i class="fa-li fa fa-check"></i> Technologies that identify and measure water levels (rivers, seas) through cameras. </li><li><i class="fa-li fa fa-check"></i> ORC recognition solutions, extracting information in a predefined format from photos and videos. </li><li><i class="fa-li fa fa-check"></i> Identification technologies through photos and videos. </li></ul>'
        }
       
    },
    ds:{
        title:{
            vi:'KHOA HỌC DỮ LIỆU',
            us:'Data science'
        },
        content:{
            vi:'Data Science được định nghĩa là tất cả những gì về thu thập, khai thác và phân tích dữ liệu để tìm ra insight giá trị. Sau đó trực quan hóa các Insight cho các bên liên quan, để chuyển hóa Insight thành hành động.',
            us:'Data Science definition includes all things relating to the process of collecting, mining, and analyzing data to find valuable insights. The insights are then visualized for stakeholders, turning them into action.'
        },
        content_list:
        {
            vi:['<div class="panel active"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Triển khai xây dựng giải pháp phân tích dữ liệu lớn. Tối ưu hóa tài nguyên tính toán trên dữ liệu quy mô lớn và phát triển các ứng dụng phân tích dự đoán trên nền tảng BigData.</li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Nghiên cứu các giải pháp biểu diễn cho dữ liệu phi cấu trúc như mạng xã hội, mạng giao thông, IoT… Tích hợp các giải pháp trích xuất đặc trưng từ dữ liệu phi cấu trúc làm dữ liệu đầu vào cho các bài toán phân tích, dự báo tổng thể.</li></ul></div>',
            '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Xây dựng, nghiên cứu phát triển các bài toán dự báo dự đoán như các bài toán về khai thác dầu khí, khí tượng thuỷ văn, phân tích mạng xã hội, chấm điểm tín dụng…</li></ul></div>'
        ],
            us:[
                '<div class="panel active"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Deploying and building big data analytics solutions. Optimize computational resources on large-scale data and develop predictive analytics applications on the BigData platform. </li></ul></div>',
                '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Researching visualization solutions for unstructured data such as social networks, transport networks, IoT... Integrating solutions to extract features from unstructured data as input data for analytical and forecasting models. </li></ul></div>',
                '<div class="panel"><ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Building, researching, and developing forecasting models to solve problems such as oil and gas exploitation, hydrometeorology, social network analysis, credit scoring... </li></ul></div>'
            ]
        },
        tree:{
            vi:['Xử lý dữ liệu lớn','Dữ liệu phi cấu trúc','Dự báo dự đoán'],
            us:['Big data','Unstructure data','Prediction']
        }

    },
    ml:{
        title:{
            vi:'HỌC MÁY CHUYÊN SÂU',
            us:'Machine Learning'
        },
        content:{
            vi:'<ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Nghiên cứu về Active Learning và Optimal Learning. <ul><li>Nghiên cứu chuyên sâu về Active Learning dự báo dự đoán như các bài toán về khai thác dầu khí, khí tượng thuỷ văn,…, trợ giúp đưa kiến thức chuyên gia vào hệ thống và tăng cường độ chính xác mô hình.</li></ul></li><li><i class="fa-li fa fa-check"></i> Nghiên cứu về các hệ thống học củng cố (Reinforcement Learning) <ul><li>Nghiên cứu chuyên sâu về các hệ thống học củng cố (Reinforcement Learning), hệ thống tự động điều khiển.</li></ul></li><li><i class="fa-li fa fa-check"></i> Nghiên cứu về học máy tự động (AutoML) <ul><li>Nghiên cứu chuyên sâu về học máy tự động (AutoML) đưa ra các giải pháp trích xuất đặc trưng, lựa chọn mô hình giúp cho quá trình triển khai các hệ thống thông minh được nhanh chóng (Auto Modeling, ML Pipeline systhesis).</li><li>Nghiên cứu chuyên sâu về học máy tự động (AutoML) đưa ra các giải pháp trích xuất đặc trưng, lựa chọn mô hình giúp cho quá trình triển khai các hệ thống thay đổi của dữ liệu (Life-Long Learning, Meta Learning).</li></ul></li></ul>',
            us:'<ul class="fa-ul"><li><i class="fa-li fa fa-check"></i> Research on Active Learning and Optimal Learning. <ul><li> In-depth research on Active Learning forecasting model to solve problems such as oil and gas exploitation, hydrometeorology, ..., assist in implementing expert knowledge into the system and enhances model accuracy. </li></ul></li><li><i class="fa-li fa fa-check"></i> Research on Reinforcement Learning <ul><li> Extensive research on reinforcement learning systems (Reinforcement Learning), automatic control systems. </li></ul></li><li><i class="fa-li fa fa-check"></i> Research on automatic machine learning (AutoML) <ul><li> Extensive research on automatic machine learning (AutoML) offers solutions for feature extraction, model selection, and rapid deployment of intelligent systems (Auto Modeling, ML Pipeline synthesis). </li><li> Extensive research on automatic machine learning (AutoML) offers solutions for feature extraction and model selection to help deploy data-changing systems (Life-Long Learning, Meta-Learning). </li></ul></li></ul>'
        }
    }
  };

  language_name: string = 'vi';
  constructor(private LanguageService: LanguageChangeSharingService) {
    this.LanguageService.getLanguageObservable().subscribe(
      (language_name: string) => {
        this.language_name = language_name;
      }
    );
  }
}
