import { Component } from "@angular/core";
import { LanguageChangeSharingService } from "@core/services/languageChange-sharing.service";

@Component({
    selector:'app-product',
    templateUrl:'./products.component.html',
    styleUrls:['products.component.scss']
})
export class ProductComponent{
    data_translate:any = {
        intro:{
            title:{
                vi:'Sản phẩm',
                us:'Product'
            },
            content:{
                vi:'Sản phẩm được xây dựng trên các nền tảng, hệ thống công nghệ có ứng dụng Trí tuệ - Nhân tạo và phân tích dữ liệu lớn.',
                us:'The product is built on technology platforms and systems with the application of Artificial Intelligence and big data analysis.'
            }
        },
        dssama:{
            title:
            {
                vi:'NỀN TẢNG PHÂN TÍCH DỮ LIỆU (DSSAMA)',
                us:'DATA ANALYSIS platform (DSSAMA)'
            },
            content:{
                vi:'<p _ngcontent-xrb-c19="">Nền tảng hỗ trợ triển khai dự án trí tuệ nhân tạo và phân tích dữ liệu lớn.</p><hr _ngcontent-xrb-c19="" class="space m"><div _ngcontent-xrb-c19="" class="row"><div _ngcontent-xrb-c19="" class="col-md-12"><ul _ngcontent-xrb-c19="" class="fa-ul"><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Cho phép triển khai nhanh và tích hợp với các hệ thống hiện hữu của khách hàng bằng việc hỗ trợ ghép nối nhanh các thành phần của quá trình phân tích dữ liệu.</li><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Cho phép trực quan hoá và kiểm soát quá trình nghiên cứu triển khai các bài toán dự đoán, dự báo. Hỗ trợ tương tác trợ giúp từ các chuyên gia AI trên mạng lưới của hệ thống.</li><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Tích hợp các công cụ mạnh về AI trên một nền tảng thống nhất bao gồm các công cụ về học máy, deep learning, bigdata…</li><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Xây dựng trên nền tảng Client/Server, triển khai trên Cloud, cho phép tuỳ biến theo nhu cầu sử dụng trên hạ tầng dùng riêng. </li></ul></div></div>',
                us:'<p _ngcontent-xrb-c15="">The platform supports project implementation of artificial intelligence and big data analysis. </p><hr _ngcontent-xrb-c15="" class="space m"><div _ngcontent-xrb-c15="" class="row"><div _ngcontent-xrb-c15="" class="col-md-12"><ul _ngcontent-xrb-c15="" class="fa-ul"><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Enables rapid deployment and integration with customers\' existing systems by supporting quick pairing of data analytics components. </li><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Allow visualization and the control of implementation of prediction and forecasting model of the research. Support interactive guidance from AI experts on the system\'s network. </li><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Integrate powerful AI tools on a unified platform including tools for machine learning, deep learning, bigdata... </li><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Built-in Client/Server platform, deployed on Cloud, allowing customization according to user needs on dedicated infrastructure. </li></ul></div></div>'
            }
        },
        project_on_dssama:{
            title:{
                vi:'Các dự án được thực hiện trên nền tảng DSSAMA',
                us:'Projects that run on the DSSAMA platform'
            },
            content:{
                vi:'<div _ngcontent-xrb-c19="" class="row"><div _ngcontent-xrb-c19="" class="col-md-12"><ul _ngcontent-xrb-c19="" class="fa-ul"><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Dự án dự báo sản lượng khai thác dầu khí.</li><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Dự án dự báo các thông số khí tượng thuỷ văn tại các trạm quan trắc.</li><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Dự án khai thác thông tin từ mạng xã hội.</li><li _ngcontent-xrb-c19=""><i _ngcontent-xrb-c19="" class="fa-li fa fa-check"></i>Tư vấn triển khai chuyển đổi số, hạ tầng dữ liệu lớn và các hệ thống thông minh.</li></ul></div></div>',

                us:'<div _ngcontent-xrb-c15="" class="row"><div _ngcontent-xrb-c15="" class="col-md-12"><ul _ngcontent-xrb-c15="" class="fa-ul"><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Oil and gas production forecasting project. </li><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Hydrometeorological parameters at monitoring stations prediction project. </li><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Social networks information extraction project. </li><li _ngcontent-xrb-c15=""><i _ngcontent-xrb-c15="" class="fa-li fa fa-check"></i>Consulting on implementation of digital transformation, big data infrastructure, and smart systems. </li></ul></div></div>'
            }
        }
    }
    
    language_name:string = 'vi'

    constructor(private LanguageService:LanguageChangeSharingService){
        this.LanguageService.getLanguageObservable().subscribe((language_name:string)=>
        {
            this.language_name = language_name
        })
    }


}