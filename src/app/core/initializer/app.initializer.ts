import { LanguageChangeSharingService } from "../services/languageChange-sharing.service";


export function appInitializer(languageService:LanguageChangeSharingService) {
    const language:any = localStorage.getItem('LANGUAGE')
    let language_parse = language == null? 'vi' : language ;
    
    languageService.Language = language_parse

    return ()  => { 
        return new Promise((resolve,reject) => {
          return setTimeout(() => resolve(true), 0);
        })
      }

}