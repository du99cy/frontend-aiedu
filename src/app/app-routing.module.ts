import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsComponent } from '@features/layouts/layouts.component';
import { BlogsComponent } from './pages/blogs/blogs.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CoursesComponent } from './pages/courses/courses.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductComponent } from './pages/products/products.component';
import { ResearchComponent } from './pages/research/research.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutsComponent,
    children: [
      {
        path: 'home',
        component:HomeComponent,
        pathMatch:'full'
      },
      {
        path: '',
        redirectTo:'/home',
        pathMatch:'full'
      },
      {
        path:'courses',
        component:CoursesComponent,
        pathMatch:'full'
      },
      {
        path:'research',
        component:ResearchComponent,
        pathMatch:'full'
      },
      {
        path:'products',
        component:ProductComponent,
        pathMatch:'full'
      },
      {
        path:'blogs',
        component:BlogsComponent,
        pathMatch:'full'
      },
      {
        path:'contacts',
        component:ContactComponent,
        pathMatch:'full'
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
