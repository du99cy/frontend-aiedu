import { AfterViewInit, Component} from '@angular/core';
import { Router } from '@angular/router';
import { LanguageChangeSharingService } from '@core/services/languageChange-sharing.service';

@Component({
  selector: 'app-headers',
  templateUrl: './headers.component.html',
  styleUrls: ['./headers.component.scss'],
})
export class HeadersComponent implements AfterViewInit{
  onButtonGroupClick($event: { target: any; srcElement: any }) {
    let clickedElement = $event.target || $event.srcElement;
    console.log(clickedElement)
    if (clickedElement.nodeName === 'BUTTON') {
      let isCertainButtonAlreadyActive =
        clickedElement.parentElement.querySelector('.active');
      // if a Button already has Class: .active
      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }

      clickedElement.className += ' active';
    }
  }

  data_translate: any = {
    home: { vi: 'Trang chủ', us: 'Home' },
    training: { vi: 'Đào tạo', us: 'Training' },
    research: { vi: 'Nghiên cứu', us: 'Research' },
    product: { vi: 'Sản phẩm', us: 'Product' },
    partner: { vi: 'Đối tác', us: 'Partner' },
    contact: { vi: 'Liên hệ', us: 'Contact' },
  };

  language_structure: any = {
    vi: { name: 'US', img: 'americanflag.jpg' },
    us: { name: 'VN', img: 'vietnamflag.png' },
  };

  language_name: string = 'vi';

  constructor(
    private languageService: LanguageChangeSharingService,
    private router: Router
  ) {
    this.language_name = this.languageService.Language;
  }

  ngAfterViewInit()
  {
    let url = this.router.url.split('/')
    //li in ul 
    let li = document.getElementById(url[1])
    //add class active 
    li?.classList.add('active')
    
  }

  ChangeLanguage() {
    this.language_name = this.language_name == 'vi' ? 'us' : 'vi';
    this.languageService.Language = this.language_name;
  }
}
